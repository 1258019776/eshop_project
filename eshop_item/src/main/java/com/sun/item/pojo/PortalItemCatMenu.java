package com.sun.item.pojo;

import java.util.List;

/**
 * 类目菜单数据类
 * @author 孙哈哈
 *
 */
public class PortalItemCatMenu {

	private String u;//url
	
	private String n;//类目名称
	
	private List<Object> i;//当有子类目时，为List<MenuCat>类型，当没有子类目时为List<String>类型，所以用List<Object>
	
	
	public String getU() {
		return u;
	}
	public void setU(String u) {
		this.u = u;
	}
	public String getN() {
		return n;
	}
	public void setN(String n) {
		this.n = n;
	}
	public List<Object> getI() {
		return i;
	}
	public void setI(List<Object> i) {
		this.i = i;
	}
	
	
}
