package com.sun.commons.pojo;

import com.sun.webmanage.model.TbItem;

public class TbItemVO extends TbItem{

	private Boolean enough;
	private String[] images;
	
	public Boolean getEnough() {
		return enough;
	}

	public void setEnough(Boolean enough) {
		this.enough = enough;
	}

	public String[] getImages() {
		return images;
	}

	public void setImages(String[] images) {
		this.images = images;
	}
	
	
}
