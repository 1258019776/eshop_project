package com.sun.commons.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @author sunhongmin
 * 前台数据列表Bean
 */
public class EasyUIDataGrid implements Serializable{

	//每页数据条数
	private List<?> rows;
	//总条数
	private long total;
	
	public EasyUIDataGrid(){}
	
	public EasyUIDataGrid(List<?> rows, long total) {
		super();
		this.rows = rows;
		this.total = total;
	}
	
	public List<?> getRows() {
		return rows;
	}
	public void setRows(List<?> rows) {
		this.rows = rows;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	
	
}
