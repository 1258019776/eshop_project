{
  "responseHeader": {
    "status": 0,
    "QTime": 269,
    "params": {
      "indent": "true",
      "q": "item_keywords:手机",
      "_": "1572526852993",
      "hl.simple.pre": "<em>",
      "hl.simple.post": "</em>",
      "hl.fl": "item_title",
      "wt": "json",
      "hl": "true"
    }
  },
  "response": {
    "numFound": 2703,
    "start": 0,
    "maxScore": 0.77291036,
    "docs": [
      {
        "id": "1039296",
        "item_title": "合约惠机测试手机（请勿下单）",
        "item_sell_point": "",
        "item_price": 269900,
        "item_image": "http://image.egou.com/jd/7f4e1eaf8f06492aaaf7a717f20344c7.jpg",
        "item_category_name": "手机",
        "_version_": 1648877809451401200
      },
      {
        "id": "143771131488369",
        "item_title": "121",
        "item_sell_point": "121",
        "item_price": 120,
        "item_image": "http://localhost:9000/images/20150724/1437711287962003.jpg",
        "item_category_name": "手机",
        "_version_": 1648877880118083600
      },
      {
        "id": "1460827382",
        "item_title": "海尔（Haier）HM-M209手机 老人机 老人手机 老年手机 直板手机 白色",
        "item_sell_point": "下单即送100京豆！",
        "item_price": 19900,
        "item_image": "http://image.egou.com/jd/80a9080245fd4281a4d40ace8ee0baf5.jpg",
        "item_category_name": "手机",
        "_version_": 1648877877483012000
      },
      {
        "id": "1458729469",
        "item_title": "海尔（Haier）HM-M209手机 老人机 老人手机 老年手机 直板手机 红色",
        "item_sell_point": "下单即送100京豆！",
        "item_price": 19900,
        "item_image": "http://image.egou.com/jd/02203e7779704ffe87ffabd451105869.jpg",
        "item_category_name": "手机",
        "_version_": 1648877877157953500
      },
      {
        "id": "1458729470",
        "item_title": "海尔（Haier）HM-M209手机 老人机 老人手机 老年手机 直板手机 黑色",
        "item_sell_point": "下单即送100京豆！",
        "item_price": 19900,
        "item_image": "http://image.egou.com/jd/5c5bb93dbe3f486eb209735770b830ba.jpg",
        "item_category_name": "手机",
        "_version_": 1648877877181022200
      },
      {
        "id": "1390218722",
        "item_title": "金国威（SanCup）D600荣耀 老人手机移动/联通2G手写手机 双卡双待 幻影金",
        "item_sell_point": "魔音手机 触屏直板老人机 手机 老人手机大字体 大按键 大音量 超长待机 老年手机男女",
        "item_price": 19900,
        "item_image": "http://image.egou.com/jd/c2f6762deaed4cbda7e126c99ea536a2.jpg",
        "item_category_name": "手机",
        "_version_": 1648877869273710600
      },
      {
        "id": "1390218723",
        "item_title": "金国威（SanCup）D600荣耀 老人手机移动/联通2G手写手机 双卡双待 钢琴黑",
        "item_sell_point": "魔音手机 触屏直板老人机 手机 老人手机大字体 大按键 大音量 超长待机 老年手机男女",
        "item_price": 19900,
        "item_image": "http://image.egou.com/jd/e2dc3218b74349cabeb4c0715776c9ed.jpg",
        "item_category_name": "手机",
        "_version_": 1648877869288390700
      },
      {
        "id": "1390218724",
        "item_title": "金国威（SanCup）D600荣耀 老人手机移动/联通2G手写手机 双卡双待 褐咖啡",
        "item_sell_point": "魔音手机 触屏直板老人机 手机 老人手机大字体 大按键 大音量 超长待机 老年手机男女",
        "item_price": 19900,
        "item_image": "http://image.egou.com/jd/b6adca68a9df41f8aa44ca12fc79e5ac.jpg",
        "item_category_name": "手机",
        "_version_": 1648877869302022100
      },
      {
        "id": "1312760511",
        "item_title": "心迪(XIND) 399D 迷你手机 移动/联通2G 双卡双待 黑色",
        "item_sell_point": "超长待机 直板按键迷你小手机 小汽车外观 儿童学生手机 女性学生手机女 老人手机 备用手机",
        "item_price": 11800,
        "item_image": "http://image.egou.com/jd/27ee0ee6f62c43ae9aa11f9fd6e8b9a9.jpg",
        "item_category_name": "手机",
        "_version_": 1648877862706479000
      },
      {
        "id": "1460869943",
        "item_title": "威铂 (v-hope) E106 迷你 电信2G商务手机 黑色",
        "item_sell_point": "迷你卡片手机 超长待机 直板按键迷你手机 儿童学生手机 商务手机 9个亲情号 老人手机",
        "item_price": 29900,
        "item_image": "http://image.egou.com/jd/1590783d39d5477aadad28af256fbd12.jpg",
        "item_category_name": "手机",
        "_version_": 1648877877495595000
      }
    ]
  },
  "highlighting": {
    "1039296": {
      "item_title": [
        "合约惠<em>机</em>测试<em>手机</em>（请勿下单）"
      ]
    },
    "1312760511": {
      "item_title": [
        "心迪(XIND) 399D 迷你<em>手机</em> 移动/联通2G 双卡双待 黑色"
      ]
    },
    "1390218722": {
      "item_title": [
        "金国威（SanCup）D600荣耀 老人<em>手机</em>移动/联通2G<em>手写手机</em> 双卡双待 幻影金"
      ]
    },
    "1390218723": {
      "item_title": [
        "金国威（SanCup）D600荣耀 老人<em>手机</em>移动/联通2G<em>手写手机</em> 双卡双待 钢琴黑"
      ]
    },
    "1390218724": {
      "item_title": [
        "金国威（SanCup）D600荣耀 老人<em>手机</em>移动/联通2G<em>手写手机</em> 双卡双待 褐咖啡"
      ]
    },
    "1458729469": {
      "item_title": [
        "海尔（Haier）HM-M209<em>手机</em> 老人机 老人<em>手机</em> 老年<em>手机</em> 直板<em>手机</em> 红色"
      ]
    },
    "1458729470": {
      "item_title": [
        "海尔（Haier）HM-M209<em>手机</em> 老人机 老人<em>手机</em> 老年<em>手机</em> 直板<em>手机</em> 黑色"
      ]
    },
    "1460827382": {
      "item_title": [
        "海尔（Haier）HM-M209<em>手机</em> 老人机 老人<em>手机</em> 老年<em>手机</em> 直板<em>手机</em> 白色"
      ]
    },
    "1460869943": {
      "item_title": [
        "威铂 (v-hope) E106 迷你 电信2G商务<em>手机</em> 黑色"
      ]
    },
    "143771131488369": {}
  }
}