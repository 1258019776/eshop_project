package com.sun.dubbo.webmanage.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.sun.dubbo.webmanage.service.TbItemParamItemDubboService;
import com.sun.webmanage.mapper.TbItemParamItemMapper;
import com.sun.webmanage.model.TbItemParamItem;
import com.sun.webmanage.model.TbItemParamItemExample;

public class TbItemParamItemDubboServiceImpl implements TbItemParamItemDubboService{
	
	@Resource
	private TbItemParamItemMapper tbItemParamItemMapper;

	@Override
	public boolean insertTbItemParamItem(TbItemParamItem record) {
		if(tbItemParamItemMapper.insertSelective(record)>0){
			return true;
		}
		return false;
	}

	@Override
	public TbItemParamItem selectTbItemParamItemByItemId(long item_id) {
		TbItemParamItemExample example = new TbItemParamItemExample();
		example.createCriteria().andItemIdEqualTo(item_id);
		List<TbItemParamItem> tbItemParamItem = tbItemParamItemMapper.selectByExampleWithBLOBs(example);
        if(tbItemParamItem!=null && !tbItemParamItem.isEmpty()){
		return	tbItemParamItem.get(0);
        }
        return null;
	}

}
