package com.sun.webmanage.service;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.webmanage.model.TbContent;

public interface TbContentService {

	EasyUIDataGrid listTbContentPage(int page,int rows,long cid);
	
	boolean saveTbContent(TbContent tbContent);
	
	boolean editTbContent(TbContent tbContent);
	
	boolean delTbContent(String ids);
	
}
